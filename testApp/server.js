var http = require('http');
var fs = require('fs');

http.createServer(function(request, response) {
	var url = request.url;
	switch(url){
		case '/':
				getStaticFileContent(response,'public/home.html','text/html');
				break;
		case '/about':
				getStaticFileContent(response,'public/about.html','text/html');
				break;
		case '/contacts':
				getStaticFileContent(response,'public/contacts.html','text/html');
				break;
		default:
			response.writeHead(404,{'Content-Type':'text/plain'});
			response.end('not found');
			
			}
			
}).listen(7000);
console.log('server running at localhost:7000');
function getStaticFileContent(response, filepath, contentType) {
	fs.readFile(filepath, function(error, data){
		if(error){
			response.writeHead(500,{'Content-Type':'text/plan'});
			response.end('500 - Internal Error.');
		}
		if(data){
			response.writeHead(200,{'Content-Type':'text/html'});
			response.end(data);
		}
	});
	
}